import 'models/discount_cards.dart';

main() {
  BronzeCard bronzeCard = BronzeCard(3, 0, 150);
  SilverCard silverCard = SilverCard(2, 600, 850);
  GoldCard goldCard = GoldCard(1, 1500, 1300);

  print("bronze card:");
  PayDesk.currentPurchase(bronzeCard);

  print("silver card");
  PayDesk.currentPurchase(silverCard);

  print("gold card");
  PayDesk.currentPurchase(goldCard);
}
