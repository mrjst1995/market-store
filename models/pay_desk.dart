part of 'discount_cards.dart';

class PayDesk {
  static void currentPurchase(var instance) {
    print("Purchase value:${instance._purchaseValue.toStringAsFixed(2)} \$");
    print(
        "Discount rate: ${(instance._discountRate * 100).toStringAsFixed(2)} %");
    print("Discount: ${(instance.calculateDiscount().toStringAsFixed(2))} \$");
    print("Total: ${(instance.calculateTotal().toStringAsFixed(2))}");
  }
}
