part of 'discount_cards.dart';

class GoldCard extends Card {
  GoldCard(int _ownerId, double _turnover, double _purchaseValue)
      : super(_ownerId, _turnover, _purchaseValue);

  @override
  calculateDiscountRate() {
    _discountRate = 0.02 + (_turnover / 100) * 0.01;
    if (_discountRate > 0.1) _discountRate = 0.1;
  }
}
