part of 'discount_cards.dart';

class BronzeCard extends Card {
  BronzeCard(int _ownerId, double _turnover, double _purchaseValue)
      : super(_ownerId, _turnover, _purchaseValue);

  @override
  calculateDiscountRate() {
    if (_turnover < 100) {
      _discountRate = 0;
    } else if (_turnover <= 300) {
      _discountRate = 0.01;
    } else {
      _discountRate = 0.025;
    }
  }
}
