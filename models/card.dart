part of 'discount_cards.dart';

abstract class Card {
  int _ownerId;
  double _turnover;
  double _purchaseValue;
  double _discountRate;

  void calculateDiscountRate();
  double calculateDiscount() => _purchaseValue * _discountRate;
  double calculateTotal() => _purchaseValue - calculateDiscount();

  Card(this._ownerId, this._turnover, this._purchaseValue) {
    calculateDiscountRate();
  }
}
