part of 'discount_cards.dart';

class SilverCard extends Card {
  SilverCard(int _ownerId, double _turnover, double _purchaseValue)
      : super(_ownerId, _turnover, _purchaseValue);

  @override
  calculateDiscountRate() {
    if (_turnover > 300) {
      _discountRate = 0.035;
    } else {
      _discountRate = 0.02;
    }
  }
}
